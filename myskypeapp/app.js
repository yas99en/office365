$(function() {
    var appId = "bde0b156-95c3-4191-9207-eb5fd0eb24f8";
    var rootEndPoint = "https://webdir.online.lync.com/autodiscover/autodiscoverservice.svc/root";

    function log(msg) {
        $("#logs").append(msg+"\n");
        console.log(msg);
    }
    
    function getHashParams() {
        var result = {};
        location.hash.substr(1).split("&").forEach(function(keyValue) {
            var splitted = keyValue.split("=", 2);
            result[splitted[0]] = splitted[1];
        });
        return result;
    }

    function getOrigin(url) {
        var aElem = $("<a>").get(0);
        aElem.href = url;
        return aElem.origin
    }

    function authenticate(resource) {
        log("authenticate", resource);
        var redirectUri = location.origin + location.pathname;
        sessionStorage["currentResource"] = resource;
        setTimeout(function() {
        location.href = "https://login.microsoftonline.com/common/oauth2/authorize"
            +"?response_type=token"
            +"&client_id=" + appId
            +"&resource=" + encodeURIComponent(resource)
            +"&redirect_uri="+encodeURIComponent(redirectUri)
        }, 5000);
    }

    function updateAccessTokens() {
        var params = getHashParams();
        var accessToken = params["access_token"];
        
        var resource = sessionStorage["currentResource"];
        
        var accessTokensStr = sessionStorage["accessTokens"];
        accessTokens = (accessTokensStr != null) ? JSON.parse(accessTokensStr): {};
        if(resource != null && accessToken != null) {
            accessTokens[resource] = accessToken;
        }
        sessionStorage["accessTokens"] = JSON.stringify(accessTokens);
        return accessTokens;
    }

    function fetchOptions(method, token, body) {
        var headers = new Headers();
        var bearer = "Bearer " + token;
        headers.append("Authorization", bearer);
        headers.append("Accept", "application/json");
        var options = {
            method: method,
            headers: headers,
        };

        if(method === "POST") {
            headers.append("Content-Type", "application/json");
            options.body = JSON.stringify(body);
        }

        return options;
    }

    function getAppEndPoint(endPoint, accessTokens) {
        console.log("getAppEndPoint", endPoint);
        var origin = getOrigin(endPoint);
        var token = accessTokens[origin];

        if(token == null) {
            authenticate(origin);
            return new Promise(function(res, rej) {
                rej(new Error("unauthenticated"));
            })
        }

        return fetch(endPoint, fetchOptions("GET", token))
            .then(res => res.json())
            .then(function (data) {
                if(data._links.applications == null) {
                    return getAppEndPoint(data._links.user.href, accessTokens);
                }
                
                var appEndPoint = data._links.applications.href;
                var appOrigin = getOrigin(appEndPoint)
                var appToken = accessTokens[appOrigin];
                if(appToken == null) {
                    return getAppEndPoint(appEndPoint, accessTokens);
                }
                return new Promise(function(res, rej) {
                    res({
                        endPoint: appEndPoint,
                        token: appToken,
                    });
                })                
            });
    }


    function getAppInstance(appEndPoint) {
        log(appEndPoint);

        var body = {
            "culture": "ja-JP",
            "endpointId": "123456",
            "userAgent": "Test App"
        };
            
        return fetch(appEndPoint.endPoint, fetchOptions("POST", appEndPoint.token, body))
            .then(res => res.json())
            .then(function (detail) {
                log("detail", detail);
                var result = {
                    endPoint: appEndPoint.endPoint,
                    token: appEndPoint.token,
                    detail: detail,
                    prefix: appEndPoint.endPoint + "/" + detail.id,
                };
                console.log("instance", JSON.stringify(result, null, 2));
                return result;
            });
    }

    getAppEndPoint(rootEndPoint, updateAccessTokens())
        .then(getAppInstance)
        .then(function (instance) {
            var body = {};
            return fetch(instance.prefix+"/me/makeMeAvailable", fetchOptions("POST", instance.token, body))
                .then(res => instance, res=>instance);
        })
        .then(function(instance) {
            log("fetching presence");
            return fetch(instance.prefix+"/presence", fetchOptions("GET", instance.token))
                .then(res => res.json())
                .then(function (data) {
                    log(JSON.stringify(data));
                    return instance;
                });
        })
        .then(function(instance) {
            return fetch(instance.prefix+"/me", fetchOptions("GET", instance.token))
                .then(res => res.json())
                .then(function (data) {
                    log(JSON.stringify(data, null, 2));
                    return instance;
                });
            
        })
        .then(function(instance) {
            return fetch(instance.prefix+"/people", fetchOptions("GET", instance.token))
                .then(res => res.json())
                .then(function (data) {
                    log(JSON.stringify(data, null, 2));
                    return instance;
                });
            
        })
        .catch(function(e) {
            if(e.message === "unauthenticated") {
                return;
            }
            throw e;
        });
});

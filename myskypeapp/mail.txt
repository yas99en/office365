

お疲れ様です

UCWA APIのエンドポイントは判明しました。
https://webpoolos2jp106.infra.lync.com/ucwa/oauth/v1/applications

取得方法の詳細は以下。
Skypeサービスは複数のプールに分散されているので、Autodiscoveryメソッド
で発見する必要があります。

https://webdir.online.lync.com/autodiscover/autodiscoverservice.svc/root
にアクセスすると、
{"_links":{
"self":{"href":"https://webdirjp1.online.lync.com/Autodiscover/AutodiscoverService.svc/root"},
"user":{"href":"https://webdirjp1.online.lync.com/Autodiscover/AutodiscoverService.svc/root/oauth/user"},
"xframe":{"href":"https://webdirjp1.online.lync.com/Autodiscover/XFrame/XFrame.html"}}}
が返ってきます。見やすくするため改行をはさんでいます。

その後の方法は複数ありますが、今は応答の"user"の方を使うことにしますが、
アクセスするために認証トークンが必要なので、まずそれを取得します。
下のURLにブラウザでアクセスします。(本来はアプリケーションからリクエストする)

client_idは事前にAzure ADに登録したアプリケーションのIDです。
アプリケーションにはSkypeの権限付与と、マニフェストでoauthAllowImplicitFlowをtrueにしてあります。
redirect_urlはアプリケーションのエンドポイントですが、この段階では適当です。
resourceの部分がこれからアクセスしたいURLです。

https://login.microsoftonline.com/common/oauth2/authorize?response_type=token&client_id=7a6d5d2d-8e9f-4d2b-bc6c-b5b65a9ea0c1&redirect_uri=http%3a%2f%2flocalhost%3a8080%2fredirect&resource=https%3a%2f%2fwebdirjp1.online.lync.com

こういう応答が返ります。
http://localhost:8080/redirect#access_token=...
&token_type=Bearer&expires_in=3599
&session_state=c589881d-b5a2-4498-a1bf-1140b1daa879

access_tokenの部分を切り出して次に進みます。

curlコマンドなどを使って先のURLにPOSTします。
curl \
--header "Authorization: Bearer ここにトークンを入れる>" \
--header "Accept: application/json" \
https://webdirjp1.online.lync.com/Autodiscover/AutodiscoverService.svc/root/oauth/user

こんな応答が返ります。"applications"の部分がAPIのエンドポイントです。
{"_links":{"self":{"href":"https://webpoolos2jp106.infra.lync.com/Autodiscover/AutodiscoverService.svc/root/oauth/user"},"applications":{"href":"https://webpoolos2jp106.infra.lync.com/ucwa/oauth/v1/applications","revision":"2"},"xframe":{"href":"https://webpoolos2jp106.infra.lync.com/Autodiscover/XFrame/XFrame.html"}}}

今回はすぐにapplicationsが返されましたが、それを得られるまで前の手順を繰り返す必要があります。

APIのエンドポイントが分かったので、認証と組み合わせてUCWAを呼んでみましたが、応答は
You do not have permission to view this directory or page using the credentials that you supplied.
です。続く

曽田




お疲れ様です

presence取得できました。
{"availability":"Online","_links":{"self":{"href":"/ucwa/oauth/v1/applications/102593783341/me/presence"}},"rel":"presence"}

取得する際は先立ってmakeMeAvailableを呼んでおく必要があります。
POST /ucwa/oauth/v1/applications/102593783341/me/makeMeAvailable
GET /ucwa/oauth/v1/applications/102593783341/presence (meは省略可)
で、自分のpresenceを取得できます。

数字の部分は、最初に
POST /ucwa/oauth/v1/applications
{ "UserAgent": "UCWA Client", "EndpointId": "1111", "Culture": "ja-JP" }
で取得できます。UserAgentとEndpointIdの値はなんでもよし。

詰まるケースとしては
Content-Typeを指定しないと
{"code":"UnsupportedMediaType","message":"The mode of communication you\u0027re trying to use isn\u0027t supported with this person or meeting."}
ブラウザにクッキーが残っていたり、パラメータを間違えて取得したトークンを使うと、
You do not have permission to view this directory or page using the credentials that you supplied.(HTMLで返る)
予めmakeMeAvailableを呼んでいないと、
https://webpoolos2jp106.infra.lync.com/ucwa/oauth/v1/applications/102223338194/me/presence
->{"code":"Conflict","subcode":"MakeMeAvailableRequired","message":"エラーが発生しました。サインインし直してください。問題が解決しない場合は、サポート チームにお問い合わせください。"}
とか。

つづく
曽田

--
遠藤 康裕

富士ゼロックスアドバンストテクノロジー株式会社
デバイスソリューション開発統括部
販社・大型個別サポート部
案件推進1G 2T
E-mail: yasuhiro.endo@fxat.co.jp
内線: 9909273
〒220-8668
神奈川県横浜市西区みなとみらい六丁目1番
Tel: 045-755-7865


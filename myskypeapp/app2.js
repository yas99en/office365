$(function() {
    var appId = "bde0b156-95c3-4191-9207-eb5fd0eb24f8";
    var rootEndPoint = "https://webdir.online.lync.com/autodiscover/autodiscoverservice.svc/root";

    function log(msg) {
        msg = (typeof msg === "string") ? msg: JSON.stringify(msg);
        $("#logs").append(msg+"\n");
        console.log(msg);
    }
    
    function getOrigin(url) {
        var aElem = $("<a>").get(0);
        aElem.href = url;
        return aElem.origin
    }

    function fetchOptions(method, token, body) {
        var headers = new Headers();
        var bearer = "Bearer " + token;
        headers.append("Authorization", bearer);
        headers.append("Accept", "application/json");
        var options = {
            method: method,
            headers: headers,
        };

        if(method === "POST") {
            headers.append("Content-Type", "application/json");
            options.body = JSON.stringify(body);
        }

        return options;
    }


    function defer() {
        var deferred = {};
        var promise = new Promise(function(res, rej) {
            deferred.res = res;
            deferred.rej = rej;
        });
        deferred.promise = promise;
        return deferred;
    }
    
    window.config = {
        clientId: appId,
        redirectUri: location.origin + location.pathname,
        // callback : callbackFunction
    };

    // function callbackFunction(errorDesc, token, error, tokenType) {
    //     console.log("callbackFunction", errorDesc, token, error, tokenType);
    // }


    var authContext = new AuthenticationContext(config);
    if(authContext.isCallback(window.location.hash)) {
        authContext.handleWindowCallback();
        return;
    }

    $("#login").click(function() {
        authContext.login();
    });

    $("#logout").click(function() {
        authContext.logOut();
    });

    
    var user = authContext.getCachedUser();
    if (user == null) {
        return;
    }

    // Use the logged in user information to call your own api
    log(JSON.stringify(user, null, 2));

    
    function acquireToken(endPoint) {
        var origin = getOrigin(endPoint);

        var d = defer();
        authContext.acquireToken(origin, function(errDesk, token, error) {
            if(error == null) {
                d.res({
                    endPoint: endPoint,
                    token: token,
                })
            } else {
                console.error(error, errDesk, origin);
                d.rej(new Error(error));
            }
        });
        return d.promise;
    }

    function discoverAppEndPoint(endPoint) {
        console.log("discoverAppEndPoint", endPoint);

        return acquireToken(endPoint)
            .then(function(result){
                return fetch(result.endPoint, fetchOptions("GET", result.token))
            })
            .then(res => res.json())
            .then(function (data) {
                if(data._links.applications == null) {
                    return discoverAppEndPoint(data._links.user.href);
                }
                
                var appEndPoint = data._links.applications.href;
                return acquireToken(appEndPoint);
            })
    }


    function getAppInstance(appEndPoint) {
        log("appEndPoint:"+JSON.stringify(appEndPoint));

        var body = {
            "culture": "ja-JP",
            "endpointId": "123456",
            "userAgent": "Test App"
        };
            
        return fetch(appEndPoint.endPoint, fetchOptions("POST", appEndPoint.token, body))
            .then(res => res.json())
            .then(function (detail) {
                console.log("detail", detail);
                return {
                    endPoint: appEndPoint.endPoint,
                    token: appEndPoint.token,
                    detail: detail,
                    prefix: appEndPoint.endPoint + "/" + detail.id,
                };
            })
    }

    discoverAppEndPoint(rootEndPoint)
        .then(getAppInstance)
        .then(function (data) {
            var body = {};
            return fetch(data.prefix+"/me/makeMeAvailable", fetchOptions("POST", data.token, body))
                .then(res => data, res=>data);
        })
        .then(function(data) {
            log("fetching presence");
            return fetch(data.prefix+"/presence", fetchOptions("GET", data.token))
        })
        .then(res => res.json())
        .then(function (data) {
            log(JSON.stringify(data, null, 2));
        })
        .catch(function(e) {
            if(e.message === "unauthenticated") {
                return;
            }
            throw e;
        });


    });

package yas99en.graphprint;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@SuppressWarnings("serial")
public class DumpServlet extends HttpServlet {

    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException{
            dump(request, response);
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException{
        dump(request, response);
    }

    public void dump(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException{

        response.setContentType("text/plain;");
        PrintWriter out = response.getWriter();

        out.println(request.getMethod());
        
        BufferedReader reader = request.getReader();
        String str = null;
        while((str = reader.readLine()) != null) {
            out.println(str);
            System.out.println(str);
        }

        out.close();
    }
}

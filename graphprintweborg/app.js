$(function() {
    var appId = "78d619c0-b5c8-4de5-8b3a-4359825dffc5"

    function log(msg) {
        msg = (typeof msg === "string") ? msg: JSON.stringify(msg);
        $("#logs").append(msg+"\n");
        $("#logs").get(0).scrollTop = $("#logs").get(0).scrollHeight - $("#logs").height();
        console.log(msg);
    }
    
    function fetchOptions(method, token, body) {
        var headers = new Headers();
        if(token != null) {
            var bearer = "Bearer " + token;
            headers.append("Authorization", bearer);
        }
        headers.append("Accept", "application/json");
        var options = {
            method: method,
            headers: headers,
        };

        if(method === "POST") {
            headers.append("Content-Type", "application/json");
            options.body = JSON.stringify(body);
        }

        return options;
    }


    function defer() {
        var deferred = {};
        var promise = new Promise(function(res, rej) {
            deferred.res = res;
            deferred.rej = rej;
        });
        deferred.promise = promise;
        return deferred;
    }

    var params = getHashParams();
    if(params.dumpUrl) {
        var dumpUrl = decodeURIComponent(params.dumpUrl);
        console.log("dumpUrl="+dumpUrl);
        sessionStorage["dumpUrl"] = dumpUrl;
        var redirectUri = location.origin + location.pathname;
        location.href = redirectUri;
        return;
    }

    
    window.config = {
        clientId: appId,
        redirectUri: location.origin + location.pathname,
    };
    console.log(window.config.redirectUri);
    

    var authContext = new AuthenticationContext(config);
    if(authContext.isCallback(window.location.hash)) {
        authContext.handleWindowCallback();
        return;
    }

    $("#login").click(function() {
        authContext.login();
    });

    $("#logout").click(function() {
        authContext.logOut();
    });

    var user = authContext.getCachedUser();
    if (user == null) {
        return;
    }

    // Use the logged in user information to call your own api
    log(JSON.stringify(user, null, 2));

    
    function getOrigin(url) {
        var aElem = $("<a>").get(0);
        aElem.href = url;
        return aElem.origin
    }

    async function acquireToken(endPoint) {
        var origin = getOrigin(endPoint);

        var d = defer();
        authContext.acquireToken(origin, function(errDesk, token, error) {
            if(error == null) {
                d.res({
                    endPoint: endPoint,
                    token: token,
                })
            } else {
                console.error(error, errDesk, origin);
                d.rej(new Error(error));
            }
        });
        return d.promise;
    }

    var graphUrl = "https://graph.microsoft.com/v1.0";

    async function callRest(endPoint, method, token, body) {
        var res  = await fetch(endPoint, fetchOptions("GET", token));
        var resObj = await res.json();
        return resObj;
    }

    function dumpObj(obj) {
        log(JSON.stringify(obj, null, 2));
    }

    function getHashParams() {
        var result = {};
        location.hash.substr(1).split("&").forEach(function(keyValue) {
            var splitted = keyValue.split("=", 2);
            result[splitted[0]] = splitted[1];
        });
        return result;
    }
    
    $("#print").on("click", function() {
        printGraph();
    });

    var dumpUrl = sessionStorage["dumpUrl"]
    if(dumpUrl != null) {
        $("#printTarget").text(dumpUrl);
    }
    
    async function printGraph() {
        try {
            var token = (await acquireToken(graphUrl)).token;
            if(window.parent != null) {
                window.parent.postMessage(token, "*");
            }
            if(window.opener != null) {
                window.opener.postMessage(token, "*");
            }

            var me = await callRest(graphUrl+"/me", "GET", token);
            dumpObj(me);


            async function callAndDump(path) {
                var data = await callRest(graphUrl+path, "GET", token);
                log("---\n"+path+"\n---\n"+JSON.stringify(data, null, 2));
            }
            
            var paths = [
                // "/me/events",
                // "/drive",
                // "/drive/root",
                // "/drive/root/children",
                // "/me/drive",
                // "/me/drive/root/children",
                // "/me/drives",
                // "/me/drive/root:/mybook.xlsx",
                // "/me/drive/root:/mybook.xlsx:/workbook/worksheets",
                // "/me/drive/root:/mybook.xlsx:/workbook/worksheets/Sheet1/usedRange",
            ];
            
            for(var i = 0; i < paths.length; i++) {
                await callAndDump(paths[i]);
            }
            
            var dumpUrl = sessionStorage["dumpUrl"]
            if(dumpUrl == null) {
                return;
            }
            
            var resObj = await callRest(graphUrl+"/me/drive/root:/mybook.xlsx:/workbook/worksheets/Sheet1/usedRange",
                                    "GET", token);
            console.log(resObj.text);
            var csv = resObj.text.map(subAry => subAry.join(",")).join("\r\n");

            csv = new Uint8Array(Encoding.convert(csv, {from: "UNICODE", to: "SJIS", type:"array"}));
            
            var formData = new FormData();
            var blob = new Blob( [csv], {type:"application/octed-stream"} );
            formData.append("file", blob, "sample.csv");
            
            fetch(dumpUrl, {
                mode: "cors",
                credentials: "include",
                method: 'POST',
                body: formData
            });
            
        } catch(e) {
            console.log(e);
        }
    }

    $("#callAndDump").click(function() {
        dumpPath();
    });
    $("#path").keydown(function(e) {
        if(e.keyCode === 13) {
            dumpPath();
        }
    });
    
    async function dumpPath() {
        var result = await acquireToken(graphUrl);
        var token = result.token;
        var path = $("#path").val();
        var data = await callRest(graphUrl+path, "GET", token);
        log("---\n"+path+"\n---\n"+JSON.stringify(data, null, 2));
    }
});

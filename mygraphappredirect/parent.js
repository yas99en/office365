$(function() {

    window.addEventListener("message", function(e) {
        var token = e.data;
        accessGraph(token);
    });
    

    
    function log(msg) {
        msg = (typeof msg === "string") ? msg: JSON.stringify(msg);
        $("#logs").append(msg+"\n");
        console.log(msg);
    }
    
    function fetchOptions(method, token, body) {
        var headers = new Headers();
        var bearer = "Bearer " + token;
        headers.append("Authorization", bearer);
        headers.append("Accept", "application/json");
        var options = {
            method: method,
            headers: headers,
        };

        if(method === "POST") {
            headers.append("Content-Type", "application/json");
            options.body = JSON.stringify(body);
        }

        return options;
    }

    
    async function callRest(endPoint, method, token, body) {
        var res  = await fetch(endPoint, fetchOptions("GET", token));
        var resObj = await res.json();
        return resObj;
    }

    function dumpObj(obj) {
        log(JSON.stringify(obj, null, 2));
    }

    function getHashParams() {
        var result = {};
        location.hash.substr(1).split("&").forEach(function(keyValue) {
            var splitted = keyValue.split("=", 2);
            result[splitted[0]] = splitted[1];
        });
        return result;
    }
    
     async function accessGraph(token) {
        try {

            var graphUrl = "https://graph.microsoft.com/v1.0";

            var me = await callRest(graphUrl+"/me", "GET", token);
            dumpObj(me);

            var events = await callRest(graphUrl+"/me/events", "GET", token);
            dumpObj(events);

            var driveRoot = await callRest(graphUrl+"/drive/root", "GET", token);
            dumpObj(driveRoot);

        } catch(e) {
            console.log(e);
        }
    }

    var params = getHashParams();
    if(params.token) {
        accessGraph(params.token);
    } else {
        var myAppUri = location.href;
        location.href = "http://localhost/~yendoh/office365/mygraphappredirect/index.html#myAppUri="+myAppUri;
    }
});

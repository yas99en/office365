$(function() {
    var appId = "78d619c0-b5c8-4de5-8b3a-4359825dffc5"

    function log(msg) {
        msg = (typeof msg === "string") ? msg: JSON.stringify(msg);
        $("#logs").append(msg+"\n");
        console.log(msg);
    }
    
    function fetchOptions(method, token, body) {
        var headers = new Headers();
        var bearer = "Bearer " + token;
        headers.append("Authorization", bearer);
        headers.append("Accept", "application/json");
        var options = {
            method: method,
            headers: headers,
        };

        if(method === "POST") {
            headers.append("Content-Type", "application/json");
            options.body = JSON.stringify(body);
        }

        return options;
    }


    function defer() {
        var deferred = {};
        var promise = new Promise(function(res, rej) {
            deferred.res = res;
            deferred.rej = rej;
        });
        deferred.promise = promise;
        return deferred;
    }
    
    
    function getHashParams() {
        var result = {};
        location.hash.substr(1).split("&").forEach(function(keyValue) {
            var splitted = keyValue.split("=", 2);
            result[splitted[0]] = splitted[1];
        });
        return result;
    }

    var params = getHashParams();
    if(params["myAppUri"]) {
        sessionStorage["myAppUri"] = params["myAppUri"];
        var redirectUri = location.origin + location.pathname;
        location.href = redirectUri;
    }

    
    window.config = {
        clientId: appId,
        redirectUri: location.origin + location.pathname,
    };

    console.log("redirectUri",location.origin + location.pathname);
    var authContext = new AuthenticationContext(config);
    if(authContext.isCallback(window.location.hash)) {
        authContext.handleWindowCallback();
        return;
    }

    $("#login").click(function() {
        authContext.login();
    });

    $("#logout").click(function() {
        sessionStorage.clear();
        authContext.logOut();
    });

    
    var user = authContext.getCachedUser();
    if (user == null) {
        console.log("no user");
        return;
    }

    // Use the logged in user information to call your own api
    log(JSON.stringify(user, null, 2));

    
    function getOrigin(url) {
        var aElem = $("<a>").get(0);
        aElem.href = url;
        return aElem.origin
    }

    async function acquireToken(endPoint) {
        var origin = getOrigin(endPoint);

        var d = defer();
        authContext.acquireToken(origin, function(errDesk, token, error) {
            if(error == null) {
                d.res({
                    endPoint: endPoint,
                    token: token,
                })
            } else {
                console.error(error, errDesk, origin);
                d.rej(new Error(error));
            }
        });
        return d.promise;
    }

    var graphUrl = "https://graph.microsoft.com/v1.0";

    // acquireToken(graphUrl)
    //     .then(function(data) {
    //         log("me");
    //         return fetch(data.endPoint+"/me", fetchOptions("GET", data.token))
    //     })
    //     .then(res => res.json())
    //     .then(function (data) {
    //         log(JSON.stringify(data, null, 2));
    //     })
    //     .catch(function(e) {
    //         if(e.message === "unauthenticated") {
    //             return;
    //         }
    //         throw e;
    //     });

    async function callRest(endPoint, method, token, body) {
        var res  = await fetch(endPoint, fetchOptions("GET", token));
        var resObj = await res.json();
        return resObj;
    }

    function dumpObj(obj) {
        log(JSON.stringify(obj, null, 2));
    }
    
    (async function() {
        try {
            var token = (await acquireToken(graphUrl)).token;
            if(window.parent != null) {
                window.parent.postMessage(token, "*");
            }
            if(window.opener != null) {
                window.opener.postMessage(token, "*");
            }

            if(sessionStorage["myAppUri"]) {
                var myAppUri = sessionStorage["myAppUri"];
                sessionStorage.clear();
                console.log("myAppUri",myAppUri + "#token=" + token);
                location.href = myAppUri + "#token=" + token;
            }
            
            // var me = await callRest(graphUrl+"/me", "GET", token);
            // dumpObj(me);

            // var events = await callRest(graphUrl+"/me/events", "GET", token);
            // dumpObj(events);

            // var driveRoot = await callRest(graphUrl+"/drive/root", "GET", token);
            // dumpObj(driveRoot);

        } catch(e) {
            console.log(e);
        }
    })();
});
